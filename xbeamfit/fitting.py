# Gaussian (3 parameters) fit
import numpy as np
import scipy
import math
from sklearn.metrics import mean_squared_error
from scipy.optimize import curve_fit
import xbeamfit as xb


def makeGaussianFit(X,Y,Y_threshold):
    
    X=np.array(X); Y=np.array(Y)
    i = np.where(Y>Y_threshold)
    X=X[i].astype(float); Y=Y[i].astype(float)
    
    # guessing initial fit parameters to converge faster
    mean = xb.statistics.findMean(X,Y)
    sigma = xb.statistics.findSigma(X,Y)[0]

    popt,pcov = curve_fit(xb.distributions.Gaussian, X, Y, p0=[np.max(Y), mean, sigma])
    popte = np.sqrt(np.diag(pcov))
    x_gaussianfit = np.linspace(min(X), max(X), 1000)
    y_gaussianfit = xb.distributions.Gaussian(x_gaussianfit, *popt)
    
    return popt,pcov,popte,x_gaussianfit,y_gaussianfit


def makeGaussianFit_restricted_sigma(X,Y,Y_threshold, sigma_restriction):
    
    popt,pcov,popte,x_gaussianfit,y_gaussianfit = makeGaussianFit(X,Y,Y_threshold) # to find the standard deviation

    ii = np.where( (X>popt[1]-sigma_restriction*popt[2]) & (X<popt[1]+sigma_restriction*popt[2]) )
    Xnew = X[ii]; Ynew = Y[ii]
    #print ("Number of initial data: %s, of removed data: %s, of restricted data: %s"%(str(len(X)), str((len(X)-len(Xnew))), str(len(Xnew))))
    
    popt_new, pcov_new = curve_fit(xb.distributions.Gaussian, Xnew, Ynew, p0=[popt[0], popt[1], popt[2]])
    return popt_new,pcov_new,Xnew,Ynew

# 5-parameters Gaussian fit (no Y_threshold)

# Thanks to Hannes, but modified
def makeGaussianFit_5_parameters(X,Y):     
    X=np.array(X); Y=np.array(Y)
    
    # guessing initial fit parameters to converge faster
    #!!!!!!!!!!!to be checked again!!!!!!!!!!!!!!!#
    indx_max = np.argmax(Y)
    mu0 = X[indx_max]
    offs0 = min(Y)
    slope = 0
    ampl = max(Y)-offs0
    window = 10
    x_tmp = X[indx_max-window:indx_max+window]; y_tmp = Y[indx_max-window:indx_max+window]
    x1 = x_tmp[np.searchsorted(y_tmp[:window], offs0+ampl/2)]
    x2 = x_tmp[np.searchsorted(-y_tmp[window:], -offs0+ampl/2)]
    FWHM = x2-x1
    sigma0 = np.abs(2*FWHM/2.355)
    ampl *= np.sqrt(2*np.pi)*sigma0
    
    popt,pcov = curve_fit(xb.distributions.Gaussian_5_parameters, X, Y, p0=[offs0,slope,ampl,mu0,sigma0])
    popte = np.sqrt(np.diag(pcov))
    x_gaussianfit = np.linspace(min(X), max(X), 1000)
    y_gaussianfit = xb.distributions.Gaussian_5_parameters(x_gaussianfit, *popt)
    
    return popt,pcov,popte,x_gaussianfit,y_gaussianfit

def makeGaussianFit_5_parameters_restricted_sigma(X,Y, sigma_restriction):     
    
    popt,pcov,popte,x_gaussianfit,y_gaussianfit = makeGaussianFit_5_parameters(X, Y)
    
    ii = np.where( (X>popt[3]-sigma_restriction*popt[4]) & (X<popt[3]+sigma_restriction*popt[4]) )
    Xnew = X[ii]; Ynew = Y[ii]
    #print ("Number of initial data: %s, of removed data: %s, of restricted data: %s"%(str(len(X)), str((len(X)-len(Xnew))), str(len(Xnew))))
    
    popt_new, pcov_new = curve_fit(xb.distributions.Gaussian_5_parameters,Xnew,Ynew,p0=[popt[0],popt[1],popt[2],popt[3],popt[4]])
    return popt_new,pcov_new, Xnew,Ynew

# Half-5-parameters Gaussian Fit
# Used to check the dispersion-free optics profiles
def makeHalfGaussianFit_5_parameters(X,Y, sigma_nr):
    
    popt,pcov,popte,x_gaussianfit,y_gaussianfit = makeGaussianFit_5_parameters(X, Y)
    
    ii = np.where(X<popt[3]+sigma_nr*popt[4]) # left side
    X = X[ii]; Y = Y[ii]
    
    popt,pcov,popte,x_gaussianfit,y_gaussianfit = makeGaussianFit_5_parameters(X,Y)
    
    return popt, pcov

def makeDoubleGaussianFit(X,Y,baseline_flag):
    try:
        X=np.array(X); Y=np.array(Y)

        indx_max = np.argmax(Y)
        mu0 = X[indx_max]
        slope = (Y[-1]-Y[0])/(X[-1]-X[0])
        window = 20
        x_tmp = X[indx_max-window:indx_max+window]
        y_tmp = Y[indx_max-window:indx_max+window]
        offs0 = min(y_tmp)
        ampl = max(y_tmp)-offs0
        x1 = x_tmp[np.searchsorted(y_tmp[:window], offs0+ampl/2)]
        x2 = x_tmp[np.searchsorted(-y_tmp[window : ], -offs0+ampl/2)]
        FWHM = x2-x1
        sigma0 = np.abs(2*FWHM/2.355)
        ampl *= np.sqrt(2*np.pi)*sigma0

        if baseline_flag == 'zero':
            popt,pcov = curve_fit(xb.distributions.doubleGaussian, X, Y, p0=[mu0,ampl,sigma0,0,max(X)])
            if abs(popt[2])<abs(popt[4]):
                A1 = popt[1]
                sig1 = abs(popt[2])
                A2 = popt[3]
                sig2 = abs(popt[4])
            else:
                A1 = popt[3]
                sig1 = abs(popt[4])
                A2 = popt[1]
                sig2 = abs(popt[2])
            mu = popt[0]
            c = 0.0
            m = 0.0

        elif baseline_flag == 'non-zero':
            popt,pcov = curve_fit(xb.distributions.doubleGaussian_7_parameters, X, Y, p0=[offs0,slope,mu0,ampl,sigma0,0,max(X)])
            if abs(popt[4])<abs(popt[6]):
                A1 = popt[3]
                sig1 = abs(popt[4])
                A2 = popt[5]
                sig2 = abs(popt[6])
            else:
                A1 = popt[5]
                sig1 = abs(popt[6])
                A2 = popt[3]
                sig2 = abs(popt[4])
            mu = popt[2]
            c = popt[0]
            m = popt[1]

        return {'c': c, 'm': m, 'mu': mu, 'A1': A1, 'sigma1': sig1, 'A2': A2, 'sigma2': sig2, 'pcov': pcov, 'popt': popt}

    except Exception as ex:

        print('Double gaussian fitting failed, here is the exception: %s'%ex)
        return {k: np.nan for k in ['c', 'm', 'mu', 'A1', 'sigma1', 'A2', 'sigma2','pcov', 'popt']}

# Q-Gaussian fit
def makeQGaussianFit(X,Y,Y_threshold, mu0=0,q0=1,beta0=1,A0=1):

    X=np.array(X); Y=np.array(Y)
    i = np.where(Y>Y_threshold)
    X=X[i].astype(float); Y=Y[i].astype(float)

    popt,pcov = curve_fit(xb.distributions.qGauss, X, Y, p0=[mu0,q0,beta0,A0])
    popte = np.sqrt(np.diag(pcov))

    x_qgaussianfit = np.linspace(min(X), max(X), 1000)
    y_qgaussianfit = xb.distributions.qGauss(x_qgaussianfit, *popt)

    return popt, pcov, popte, x_qgaussianfit, y_qgaussianfit

# Exponential fit
def makeExponentialFit(X,Y):

    X = np.array(X); Y = np.array(Y)

    popt, pcov = curve_fit(xb.distributions.exponential_func, X, Y, p0=(1.0, np.max(Y)/np.max(X)/10.0, 0.0))

    xout = np.linspace(np.min(X), np.max(X), 1000)
    yout = xb.distributions.exponential_func(xout, popt[0], popt[1], popt[2])

    return popt, pcov, xout, yout

# Linear fit
def linfit_with_errors(X, Y, X_threshold, x_fit_nr):
    if X_threshold != 'none':
        i = np.where(np.array(X)>X_threshold)
        X=np.array(X)[i]; Y=np.array(X)[i]
    else:
        X=np.array(X); Y=np.array(Y)

    p,e = np.polyfit(X, Y, 1, cov=True);
    pe=np.sqrt(np.diag(e))
    x_fit = np.linspace(np.min(X), np.max(X), x_fit_nr)
    y_fit = [p[0]*xx+p[1] for xx in x_fit]

    return p, pe, x_fit, y_fit

# Quadratic fit
def quadfit_with_errors(X, Y, X_threshold, x_fit_nr):
    if X_threshold != 'none':
        i = np.where(np.array(X)>X_threshold)
        X=np.array(X)[i]; Y=np.array(X)[i]
    else:
        X=np.array(X); Y=np.array(Y)

    p,e = np.polyfit(X, Y, 2, cov=True)
    pe=np.sqrt(np.diag(e))
    x_fit = np.linspace(np.min(X), np.max(X), x_fit_nr)
    y_fit = [p[0]*xx**2+p[1]*xx+p[2] for xx in x_fit]

    return p, pe, x_fit, y_fit

