import numpy as np
import scipy
import math
from sklearn.metrics import mean_squared_error
from scipy.optimize import curve_fit


# Simple Gaussian function (3 parameters)
def Gaussian(x, A, mu, sig):
    """Gaussian(x, A, mu, sig)
    A is the amplitude
    mu is the center
    sig is the sigma
    """
    return (A/np.sqrt(2*np.pi)/sig*np.exp(-np.power(x - mu, 2.)/
           (2 * np.power(sig, 2.))))

# Gaussian function with 5-parameters (non-zero baseline)
def Gaussian_5_parameters(x, c, m, A, mu, sig):
    """gaussian_5_parameter(x, c, m, A, mu, sig)"""
    return (c+m*x+A/np.sqrt(2*np.pi)/sig*np.exp(-np.power(x - mu, 2.)/
           (2 * np.power(sig, 2.))))


# Double Gaussian with the same center (5-parameters)
def doubleGaussian(x, mu, A1, sig1, A2, sig2):
    gauss1 = A1/np.sqrt(2*np.pi)/sig1*np.exp(-np.power(x - mu, 2.)/(2*np.power(sig1, 2.)))
    gauss2 = A2/np.sqrt(2*np.pi)/sig2*np.exp(-np.power(x - mu, 2.)/(2*np.power(sig2, 2.)))
    return (gauss1+gauss2)

# Double Gaussian (7-parameters)
def doubleGaussian_7_parameters(x, A1, mu1, sig1, A2, mu2, sig2):
    gauss1 = A1/np.sqrt(2*np.pi)/sig1*np.exp(-np.power(x - mu1, 2.)/(2*np.power(sig1, 2.)))
    gauss2 = A2/np.sqrt(2*np.pi)/sig2*np.exp(-np.power(x - mu2, 2.)/(2*np.power(sig2, 2.)))
    return (gauss1+gauss2)

# Double Gaussian (7-parameters) and non-zero baseline
def doubleGaussian_9_parameters(x, c, m, A1, mu1, sig1, A2, mu2, sig2):
    gauss1 = A1/np.sqrt(2*np.pi)/sig1*np.exp(-np.power(x - mu1, 2.)/(2*np.power(sig1, 2.)))
    gauss2 = A2/np.sqrt(2*np.pi)/sig2*np.exp(-np.power(x - mu2, 2.)/(2*np.power(sig2, 2.)))
    return (c+m*x)+(gauss1+gauss2)

    # Q-Gaussian
def _Cq(q):
    Gamma = math.gamma
    if q<0.99415629720:
        return (2.0*np.sqrt(np.pi))/((3.0-q)*np.sqrt(1-q))*(Gamma(1.0/(1.0-q)))/Gamma((3.0-q)/2.0/(1.0-q))
    elif q<1.005827 and q>0.99415629720:
        return np.sqrt(np.pi)
    elif (q>1.005827 and q<3.0):
        return (np.sqrt(np.pi)*Gamma((3.0-q)/2.0/(q-1.0)))/(np.sqrt(q-1.0)*Gamma(1.0/(q-1.0)))
    else:
        raise Exception('q<3')

def _eq(x,q):
    if (q!=1 and (1+(1-q)*x)>0):
        return (1+(1-q)*x)**(1/(1-q))
    elif q==1:
        return np.exp(x)
    else:
        return 0.0**(1.0/(1.0-q))

def qGauss(x, mu, q, b, A):
    return A*np.sqrt(b)/_Cq(q)*_eq(-b*(x-mu)**2,q)

def qGauss_nonZeroBaseline(x, mu, q, b, A, c, m):
    return A*np.sqrt(b)/_Cq(q)*_eq(-b*(x-mu)**2,q)+c+m*x

# Q-Gaussian (Guido's implementation)
def _Cq2(q):
    Gamma = scipy.special.gamma
    if q<1:
        return (2*np.sqrt(np.pi))/((3-q)*np.sqrt(1-q))*(Gamma((1)/(1-q)))/(Gamma((3-q)/(2*(1-q))))
    elif q==1:
        return np.sqrt(np.pi)
    elif q<3:
        return (np.sqrt(np.pi))/(np.sqrt(q-1))*(Gamma((3-q)/(2*(q-1))))/(Gamma((1)/(q-1)))
    else:
        raise Exception('Please q<3!!!')

def _eq2(x,q):
    if q==1:
        return np.exp(x)
    elif (1+(1-q)*x)>0:
        return (1+(1-q)*x)**(1/(1-q))
    else:
        return 0

def qGauss2(x, mu, q, b, A):
    x, mu, q, b, A
    return A*np.sqrt(b)/_Cq2(q)*_eq2(-b*(x-mu)**2,q)

# Exponential
def exp_func(x, a, b, c):
        return a*np.exp(-b*x)+c