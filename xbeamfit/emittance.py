

import numpy as np
import scipy
import math
from sklearn.metrics import mean_squared_error
from scipy.optimize import curve_fit

# Normalized emittance and error for the WS
# Inputs the betatronic beam size, the beta Twiss and the relativistic factors
def emittance_WS(betatronic_beam_size_mm, betatronic_beam_size_mm_error,
                 beta_Twiss_m, beta_rel, gamma_rel):
    
    betatronic_beam_size_m = betatronic_beam_size_mm/1000.0; betatronic_beam_size_m_error = betatronic_beam_size_mm_error/1000.0
    
    emittance_geom       = betatronic_beam_size_m**2/beta_Twiss_m # units of meters*rad
    emittance_geom_error = emittance_geom*(2.0*betatronic_beam_size_m_error/betatronic_beam_size_m)
    
    emittance_norm       = emittance_geom*beta_rel*gamma_rel*1e6 # units of um
    emittance_norm_error = emittance_geom_error*beta_rel*gamma_rel*1e6
    
    return emittance_norm, emittance_norm_error


# Normalized emittance for the three SEM Grids
# Emittance computation using the 3-SEM Method (more info: https://cas.web.cern.ch/sites/cas.web.cern.ch/files/lectures/dourdan-2008/braun-emittance.pdf)
# Give as inputs only the three betatronic beam sizes and it returns the normalized emittance and the optics function at the reference point (here: BTM.SGH01)
def emittance_SEM(sigma_bet_SEM1_mm, sigma_bet_SEM2_mm, sigma_bet_SEM3_mm, La=1, Lb=1, Lc=1, beta_rel=.99, gamma_rel=27):
    
    x = [sigma_bet_SEM1_mm**2, sigma_bet_SEM2_mm**2, sigma_bet_SEM3_mm**2]
    

    Row_1 = [1, -2*La, La**2]; Row_2 = [1, -2*Lb, Lb**2]; Row_3 = [1, -2*Lc, Lc**2];
    M = np.matrix( [Row_1,Row_2,Row_3]) # transfer matrix (drift space between the SEMs)
    out = np.linalg.solve(M, x) # solve the system (Braun, slide 21)
    beta_emit  = out[0] 
    alpha_emit = out[1]
    gamma_emit = out[2]
    
    emittance_geom = math.sqrt( beta_emit*gamma_emit - alpha_emit**2.0) # RMS geometrical emittance
    emittance_norm = emittance_geom*beta_rel*gamma_rel # Normalized Emittance
    
    # Twiss parameters at the reference point (here SEM1 because La=0) as calculated from the 3-SEM Method
    beta_Twiss_m  = beta_emit  / emittance_geom
    alpha_Twiss_m = alpha_emit / emittance_geom
    gamma_Twiss_m = gamma_emit / emittance_geom
    #print ('Beta and alpha twiss parameters calculated: ' + str(betaTwiss) + ', ' + str(alphaTwiss))
    
    return emittance_norm, beta_Twiss_m, alpha_Twiss_m, gamma_Twiss_m


