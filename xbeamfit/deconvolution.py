# Assuming all profiles are Gaussian
# The dispersion must be given in meters.
import xbeamfit as xb
from scipy import interpolate
import numpy as np

def gaussian_subtraction(measured_beam_size_mm, measured_beam_size_mm_error,
                         dispersion_m, dispersion_m_error,
                         dp_p):
    
    betatronic_beam_size_mm       = np.sqrt( measured_beam_size_mm**2.0 - (dispersion_m*1000.0*dp_p)**2.0 )
    betatronic_beam_size_mm_error = (1.0/betatronic_beam_size_mm)*np.sqrt((measured_beam_size_mm*measured_beam_size_mm_error)**2  +  ((dp_p)**2*1000*dispersion_m*dispersion_m_error*1000)**2 )
    
    return betatronic_beam_size_mm, betatronic_beam_size_mm_error

# Assuming only the Betatronic profile to be Gaussian
# No errors (yet)
def deconvolution(beam_position_mm, beam_profile_arb_unit,
                  dispersion_m,
                  off_momentum_distribution_arb_unit, deltaP_P):
    
    x_raw = beam_position_mm; y_raw = beam_profile_arb_unit
    dispersion_mm = dispersion_m*1e3 # m to mm
    # The old version makes only a simple Gaussian fit. What to use? Very important for the third grid (which has negative baseline)
    fit_to_make='Gaussian'
    
    #-------------------------------Measured beam profile---------------------------------------------------#
    # Step 1: make 5-parameters gaussian fit
    if fit_to_make=='5-Gaussian':
        popt,pcov,popte,x_gaussianfit,y_gaussianfit = xb.fitting.makeGaussianFit_5_parameters(x_raw, y_raw) 
        x1 = x_raw; y1 = xb.distributions.Gaussian_5_parameters(x1, popt[0], popt[1], popt[2], popt[3], popt[4])
    elif fit_to_make=='Gaussian':
        popt,pcov,popte,x_gaussianfit,y_gaussianfit = xb.fitting.makeGaussianFit(x_raw, y_raw, -10.0)
        x1 = x_raw; y1 = xb.distributions.Gaussian(x1, popt[0], popt[1], popt[2])
    elif fit_to_make=='Q-Gaussian':
        popt,pcov = xb.fitting.makeQGaussianFit(x_raw, y_raw, -10.0, [0.0, 0.5, 0.1, 1.0])
        x1 = x_raw; y1 = xb.distributions.qGauss(x1, *popt)
    position_step1_raw = x1; profile_step1_5GaussianFit=y1
    # Step 2: center and drop baseline 
    if fit_to_make=='5-Gaussian':
        x2 = x_raw - popt[3]; y2 = y_raw - popt[0]-popt[1]*x_raw
    elif fit_to_make=='Gaussian':
        x2 = x_raw - popt[1]; y2 = y_raw
    elif fit_to_make=='Q-Gaussian':
        x2 = x_raw - popt[0]; y2 = y_raw
    position_step2_centering_mm=x2; profile_step2_dropping_baseline=y2
    # Step 3: interpolate
    f = interpolate.interp1d(x2, y2, bounds_error=0, fill_value=0)
    if fit_to_make=='5-Gaussian': 
        limit = 5*popt[4]; 
    elif fit_to_make=='Gaussian': 
        limit = 5*popt[2]; 
    elif fit_to_make=='Q-Gaussian':
        limit = 5*(popt[2]*(5.0-3.0*popt[1]))**(-0.5)
    x3 = np.linspace(-limit, limit, 1000); y3 = f(x3)
    position_step3_interpolation_mm=x3; profile_step3_interpolation=y3
    # Step 4: normalize profile
    y4 = y3/np.trapz(y3, x3)
    #y4 = y4/np.max(y4) # extra line
    profile_step4_normalization=y4
    # Step 5: make symmetric
    y5 = (y4+y4[::-1])/2
    profile_step5_symmetric=y5
    
    #-------------------------------Measured dispersive profile-----------------------------------------------#
    # Step 1: normalize off momentum distribution profile
    dispersive_position_step1_raw_mm=deltaP_P*dispersion_mm # useful for the normalization and the interpolation below
    dispersive_profile_step1_normalized=off_momentum_distribution_arb_unit/np.trapz(off_momentum_distribution_arb_unit,dispersive_position_step1_raw_mm)
    #dispersive_profile_step1_normalized = dispersive_profile_step1_normalized/np.max(dispersive_profile_step1_normalized) # extra line
    # Step 2: interpolate
    f = interpolate.interp1d(dispersive_position_step1_raw_mm,dispersive_profile_step1_normalized, bounds_error=0, fill_value=0)
    dispersive_position_step2_mm=position_step3_interpolation_mm # same as position as of the beam profile
    dispersive_step2_interpolation=f(dispersive_position_step2_mm)
    # Step 3: symmetric
    dispersive_step3_symmetric=(dispersive_step2_interpolation+dispersive_step2_interpolation[::-1])/2
    
    #-------------------------------Convolution--------------------------------------------------------------#
    def myConvolution(position_step3_interpolation_mm, sigma): # definition of a deconvolution function
        myConv=np.convolve(dispersive_step3_symmetric, xb.distributions.Gaussian(position_step3_interpolation_mm,1,0,sigma), 'same')
        myConv/=np.trapz(myConv, position_step3_interpolation_mm) # normalize
        return myConv 
    def myError(sigma):
        myConv = myConvolution(position_step3_interpolation_mm, sigma)
        aux=myConv-profile_step5_symmetric
        return np.std(aux), aux, myConv
    
    #----------------------------Find Sigma and emittance-----------------------------------#
    # Fit the measured profile with a convolution function between the dispersive profile and a simple gaussian,
    # having a single parameter (the unknown beam size sigma)
    popt,pcov = xb.fitting.curve_fit(myConvolution,position_step3_interpolation_mm, profile_step5_symmetric, p0=[1]) 
    sigma=popt[0]
    
    a = myError(sigma)
    
    
    return { 'betatronic_beam_size_mm':sigma,
             'beam_position_mm':position_step3_interpolation_mm, 'beam_profile_arb_unit': profile_step5_symmetric, 
             'dispersive_position_mm':dispersive_position_step2_mm, 'dispersive_profile':dispersive_step3_symmetric,
             'profile_raw': profile_step4_normalization,
             'convolutionBackComputed':myConvolution(position_step3_interpolation_mm,sigma),
             'betatronicProfile':xb.distributions.Gaussian(position_step3_interpolation_mm,1,0,sigma),
             'a':a
           }
