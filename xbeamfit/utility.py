import numpy as np
import scipy
import math
from sklearn.metrics import mean_squared_error
from scipy.optimize import curve_fit
import xbeamfit as xb


def findBaseline(x,y,left_x_range,right_x_range):
    x = np.array(x)
    y = np.array(y)
    
    xleft_mask = np.where((x>left_x_range[0]) & (x<left_x_range[1]))
    xright_mask = np.where((x>right_x_range[0]) & (x<right_x_range[1]))
    
    xbaseline = list(x[xleft_mask])+list(x[xright_mask])
    ybaseline = list(y[xleft_mask])+list(y[xright_mask])
    
    p,pe,xfit,yfit = xb.fitting.linfit_with_errors(xbaseline,ybaseline,'none',len(x))
    
    return {'offset_wotilt': np.mean(ybaseline), 'offset_wotilt_err': np.std(ybaseline),
            'offset_wtilt': p[1], 'offset_wtilt_err': pe[1], 'tilt': p[0], 'tilt_err': pe[0],
            'xbaseline': xbaseline, 'ybaseline': ybaseline, 'xfit': xfit, 'yfit': yfit}

def rel_factors(rest_mass, kinetic_energy_GeV):
    gamma_rel = (kinetic_energy_GeV + rest_mass)/rest_mass
    beta_rel  = np.sqrt(1-1/(gamma_rel**2.0))
    return beta_rel, gamma_rel

def momentum_2_KE(rest_mass, momentum_GeV):
    return (np.sqrt(momentum_GeV**2.0+rest_mass**2.0)-rest_mass)


