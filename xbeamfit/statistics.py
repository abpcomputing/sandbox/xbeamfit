# Statistics
import numpy as np
from sklearn.metrics import mean_squared_error

def findMean(x,y):
    return sum(x*y)/sum(y)

def findSigma(x,y):
    mu = findMean(x,y)
    sigma = np.sqrt( sum((x-mu)**2*y) / sum(y) )
    sigma_error = sigma/len(x)
    return sigma, sigma_error

def second_moment_2(values, weights, sig, n_sigmas=6.,absolute=None):
    if absolute:
        a=np.where((values<absolute) & (values>-absolute))[0]
    else:
        a=np.where((values<n_sigmas*sig) & (values>-n_sigmas*sig))[0]
    values=values[a]
    weights=weights[a]
    weighted_average = np.average(values, weights=weights)
    second_moment = np.sqrt(np.average((values-weighted_average)**2, weights=weights))
    second_moment_error = second_moment/len(values) # standard deviation of the standard deviation
    return second_moment,second_moment_error

# Mean Squared Root Error (RMSE) and relative RMSE (rRMSE)
def rmse(y_data, y_fit):
    RMSE  = np.sqrt(mean_squared_error(y_data, y_fit))
    rRMSE = RMSE/(np.mean(y_data))*100
    return RMSE, rRMSE
