from setuptools import setup, find_packages

#########
# Setup #
#########

setup(
    name='xbeamfit',
    version='0.0.1',
    description='A package for fitting the beam profiles',
    url='',
    author='Tirsi Prebibaj',
    packages=find_packages(),
    install_requires=[
        'numpy',
        ]
    )

