# %% 
import numpy as np
import xbeamfit as xb

def test_Gaussian():
    assert xb.distributions.Gaussian(0, 0, 0, 1) == 0


# %%
def test_Gaussian_fit():
    for i in range(10):
        A = np.random.random()*10
        mu = (np.random.random()-.5)*10
        sig = np.random.random()*10

        x_range = np.linspace(-5,5,100)
        y_range = [xb.distributions.Gaussian(x, A=A, mu=mu, sig=sig) 
                for x in x_range]

        out = xb.fitting.makeGaussianFit(list(x_range),list(y_range),Y_threshold=0)

        assert np.isclose(out[0][0], A)
        assert np.isclose(out[0][1], mu)
        assert np.isclose(out[0][2], sig)

def test_particle_kinematic():
    test_dict = {'totalEnergy_GeV': 7000.000062882465,
    'kinetikEnergy_GeV': 6999.061790794304,
    'pc_GeV': 7000,
    'restEnergy_GeV': 0.93827208816,
    'relativisticBeta': 0.9999999910167908,
    'relativisticBetaGamma': 7460.522473526162,
    'relativisticGamma': 7460.522540545597,
    'elementaryCharge': 1.0,
    'magneticRigidity_Tm': 23349.486663870644}

    my_dict = xb.particle.setPc_GeV(7000)

    for kk  in my_dict.keys():
        assert test_dict[kk] == my_dict[kk]
# %%
