# %% 
import numpy as np
import xbeamfit as xb
from matplotlib import pyplot  as plt

x_range = np.linspace(-5,5,100)
y_range = [xb.distributions.Gaussian(x, A=1, mu=0, sig=2) 
           for x in x_range]
plt.plot(x_range,y_range)
plt.show()
# %%
y_range = [xb.distributions.Gaussian_5_parameters(x, c=0, m=.01, A=1, mu=0, sig=1) 
           for x in x_range]
plt.plot(x_range,y_range)

# %%
y_range = [xb.distributions.qGauss(x, mu=0, q=1, b=1, A=1)
           for x in x_range]
plt.plot(x_range, y_range)

y_range2 = [xb.distributions.qGauss2(x, mu=0, q=1, b=1, A=1)
           for x in x_range]
plt.plot(x_range, y_range2,'.')


# %%
y_range = [xb.distributions.doubleGaussian(x, mu=0, A1=1, sig1=1, A2=2, sig2=2)
           for x in x_range]
plt.plot(x_range,y_range)
# %%
import scipy.stats as st

my_mu = 2.16
my_sig = 1.23

class my_pdf(st.rv_continuous):
    def _pdf(self,x):
        # use normalized distributions
        return xb.distributions.Gaussian(x, A=1, mu=my_mu, sig=my_sig)

my_cv = my_pdf( name='my_pdf')

# it can be slow
plt.hist(my_cv.rvs(size=1000),30)
plt.show()
assert np.isclose(my_cv.mean(), my_mu)
assert np.isclose(my_cv.std(), my_sig)


# %%
for i in range(10):
    A = np.random.random()*10
    mu = (np.random.random()-.5)*10
    sig = np.random.random()*10

    x_range = np.linspace(-5,5,100)
    y_range = [xb.distributions.Gaussian(x, A=A, mu=mu, sig=sig) 
            for x in x_range]
    out = xb.fitting.makeGaussianFit(list(x_range),list(y_range),Y_threshold=0)

    assert np.isclose(out[0][0], A)
    assert np.isclose(out[0][1], mu)
    assert np.isclose(out[0][2], sig)

# %%
