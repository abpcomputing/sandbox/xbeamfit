# xbeamfit

A minimum package to fit beam transverse and longitudinal beam profiles.

It mostly based on the contribution of Tirsi Prebibaj.

You can test it by 
```bash
mkdir test_xbeamfit && cd test_xbeamfit
git clone ssh://git@gitlab.cern.ch:7999/abpcomputing/sandbox/xbeamfit.git
source xbeamfit/make_it
```
